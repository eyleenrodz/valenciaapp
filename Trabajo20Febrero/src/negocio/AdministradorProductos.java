/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import DTO.Producto;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author eyleent.rodriguezr
 */
public class AdministradorProductos {

    private final List<Producto> listaProductos;

    public AdministradorProductos() {
        this.listaProductos = new ArrayList<>();
    }

    public void crearProducto(String nombreProducto, int cantidad, double precio, double monto) {
        Producto producto = new Producto(listaProductos.size() + 1, nombreProducto, cantidad, precio, monto);
        listaProductos.add(producto);
    }

    public boolean editarProducto(int id, String nombreProducto, int cantidad, double precio, double monto) {
        for (Producto producto : listaProductos) {
            if (producto.getId() == id) {
                producto.setNombre(nombreProducto);
                producto.setCantidad(cantidad);
                producto.setPrecio(precio);
                producto.setMonto(monto);
                return true;
            }
        }
        return false;
    }

    public boolean editarCantidadProducto(int id, int cantidad) {
        for (Producto producto : listaProductos) {
            if (producto.getId() == id) {
                producto.setCantidad(producto.getCantidad()-cantidad);
                return true;
            }
        }
        return false;
    }

    public boolean eliminarProducto(int id) {
        for (Iterator<Producto> iterator = listaProductos.iterator(); iterator.hasNext();) {
            Producto producto = iterator.next();
            if (producto.getId() == id) {
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    public Producto consultar(int id) {
        for (Producto producto : listaProductos) {
            if (producto.getId() == id) {
                return producto;
            }
        }
        return null;
    }

    public List<Producto> consultarProductos() {
        return Collections.checkedList(listaProductos, Producto.class);
    }

}
