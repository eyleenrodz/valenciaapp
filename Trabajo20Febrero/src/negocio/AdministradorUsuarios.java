/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import DTO.Cliente;
import constantes.UsuarioAdministrador;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author eyleent.rodriguezr
 */
public class AdministradorUsuarios {

    private final List<Cliente> clientes;

    public AdministradorUsuarios() {
        clientes = new ArrayList<>();
        clientes.add(UsuarioAdministrador.ADMINISTRADOR.getCliente());
    }

    public void crearUsuario(String nombreUsuario) {
        Cliente nuevoUsuario = new Cliente(clientes.size() + 1, nombreUsuario);
        clientes.add(nuevoUsuario);
    }

    public boolean editarUsuario(int id, String nombreUsuario) {
        for (Cliente cliente : clientes) {
            if (cliente.getIdCliente() == id) {
                cliente.setNombreCliente(nombreUsuario);
                return true;
            }
        }
        return false;
    }

    public boolean eliminarUsuario(int id) {
        for (Iterator<Cliente> iterator = clientes.iterator(); iterator.hasNext();) {
            Cliente cliente = iterator.next();
            if (cliente.getIdCliente() == id) {
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    public List<Cliente> consultarUsuarios() {
        return Collections.checkedList(clientes, Cliente.class);
    }

    public Cliente consultarUsuario(int id) {
        for (Cliente cliente : clientes) {
            if (cliente.getIdCliente() == id) {
                return cliente;
            }
        }
        return null;
    }
}
