/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import DTO.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author eyleent.rodriguezr
 */
public class AdministradorVentas {
  private final List<Venta> ventas;

  public AdministradorVentas() {
      ventas = new ArrayList<>();
  }

  public List<Venta> consultarVentas() {
      return Collections.checkedList(ventas, Venta.class);
  }

  public void agregarVenta(Venta venta){
    venta.setId(ventas.size() + 1);
    ventas.add(venta);
  }


}
