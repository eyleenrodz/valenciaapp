/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Quiz;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.TreeSet;

/**
 *
 * @author practica1
 */
public class Operaciones {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String[] datosSTR = {"1"};
        Integer contador = 0;
        do {
            TreeSet<Integer> datos = new TreeSet<>();
            String reg = "((?<=[<=|>=|==|\\+|\\*|\\-|<|>|/|=])|(?=[<=|>=|==|\\+|\\*|\\-|<|>|/|=]))";
            datosSTR = sc.nextLine().trim().split(reg);
            Integer resultado = 0;
            
            if(datosSTR.length < 5){
                break;
            }

            if (datosSTR[1].equals("+")) {
                resultado = Integer.parseInt(datosSTR[0]) + Integer.parseInt(datosSTR[2]);
                if (isNumeric(datosSTR[4]) && resultado == Integer.parseInt(datosSTR[4])) {
                    contador++;
                }
            } else if (datosSTR[1].equals("-")) {
                resultado = Integer.parseInt(datosSTR[0]) - Integer.parseInt(datosSTR[2]);

                if (isNumeric(datosSTR[4]) && resultado == Integer.parseInt(datosSTR[4])) {
                    contador++;
                }
            } else if (datosSTR[1].equals("/")) {
                resultado = Integer.parseInt(datosSTR[0]) / Integer.parseInt(datosSTR[2]);
                if (isNumeric(datosSTR[4]) && resultado == Integer.parseInt(datosSTR[4])) {
                    contador++;
                }
            } else if (datosSTR[1].equals("*")) {
                resultado = Integer.parseInt(datosSTR[0]) * Integer.parseInt(datosSTR[2]);
                if (isNumeric(datosSTR[4]) && resultado == Integer.parseInt(datosSTR[4])) {
                    contador++;
                }
            }

        } while (datosSTR.length != 0);

        System.out.println(contador);
    }

    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }
}
