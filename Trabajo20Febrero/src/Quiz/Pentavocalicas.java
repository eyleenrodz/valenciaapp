/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Quiz;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author practica1
 */
public class Pentavocalicas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        String[] vocales = {"a", "e", "i", "o", "u"};
        List<String> resp = new ArrayList();
        int pruebas = 0;
        try{
            pruebas = Integer.parseInt(input);
        }
        catch(Exception ex){
            return;
        }     
        int cont = 0;
        for (int i = 0; i < pruebas; i++) {
            input = sc.nextLine();
            for (int j = 0; j < 5; j++) {
                if(!input.contains(vocales[j])) break;
                else cont++;
            }
            if(cont == 5) 
                resp.add("SI");
            else 
                resp.add("NO");
            cont = 0;
        }
        resp.forEach(r-> System.out.println(r));
        
    }
    
}
