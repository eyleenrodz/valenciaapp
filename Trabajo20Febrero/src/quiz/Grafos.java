/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz;

/**
 *
 * @author practica1
 */
public class Grafos {

    static int Visitados[] = {0, 0, 0, 0, 0, 0, 0};

    static void DFS(int V, int mat[][]) {
        Visitados[V] = 1;  	//El v�rtice V enviado como par�metro es  				//visitado
        System.out.print(V + " ");        	//Se imprime
        int N = mat[0].length;
        for (int W = 0; W < N; W++) //Se buscan los adyacentes de V
        {
            if (mat[V][W] == 1) //Se encontr� con un adyacente(W)
            {
                if (Visitados[W] == 0) //Si no ha sido visitado
                {
                    DFS(W, mat);	  //Llamado recursivo
                }
            }
        }
    }
    //La cola se va a manejar como un vector desde la posici�n 1
    static int Cola[] = new int[100];

//Variables para el manejo de la cola:
    static int total = 0;	   //Indica el total de elementos de la cola
    static int primero = 0;
    /*Indica la posici�n del pr�ximo elemento que 		           debe ser retirado de la cola...*/
    static int ultimo = 0;

    /*Indica la posici�n del �ltimo elemento que 			     ingres� en la cola*/

    static void BFS(int V, int mat[][]) {
        int Z;
        int N = 6;
        //Se visita e imprime el elemento enviado
        Visitados[V] = 1;
        System.out.println(V);
        //Se lleva el elemento a la cola
        total++;
        primero++;
        ultimo++;
        Cola[ultimo] = V;
        //Mientras la cola no est� vac�a:
        while (total > 0) {
            //Extraer pr�ximo elemento de la cola
            Z = Cola[primero];
            primero++;
            /*Se avanza al pr�ximo elemento que  
                debe ser retirado de la cola*/
            total--;
            /*Se decrementa el total de 
                elementos de la cola*/
            //Buscar los W adyacentes a Z: 
            for (int W = 0; W < N; W++) {
                //System.out.println(mat[Z][W]);
                if (mat[Z][W] == 1) //Encontr� un adyacente
                //System.out.println(Visitados[W]);
                {
                    if (Visitados[W] == 0) { //Si no ha sido visitado
                        Visitados[W] = 1;	 //W es visitado 
                        System.out.print(W + " ");		 //Se imprime
                        //Llevar W a la cola
                        total++;
                        /*Aumenta el # de elementos 				        en la cola */
                        ultimo++;
                        /*Incrementa ultimo para entrar 				  un elemento a la cola*/
                        Cola[ultimo] = W; //Se ingresa W a la cola
                    }
                }
            }

        }
    }

    public static void main(String[] args) {
//        int[][] mat = {{0, 1, 1, 0, 0, 0}, {1, 0, 0, 1, 0, 0}, {1, 0, 0, 0, 0, 0},
//        {0, 1, 0, 0, 1, 1}, {0, 0, 0, 1, 0, 1}, {0, 0, 0, 1, 1, 0}};
        
        int[][] mat = {{0, 0, 1, 1, 1, 0, 0}, {0, 0, 0, 1, 0, 0, 0}, {1, 0, 0, 0, 0, 0, 0},
        {1, 1, 0, 0, 0, 1, 0}, {1, 0, 0, 0, 0, 1, 0}, {0, 0, 0, 1, 1, 0, 1}, {0, 0, 0, 0, 0, 1, 0}};
        
        System.out.println("DFS");
        DFS(0, mat);
        System.out.println();
        for (int i = 0; i < Visitados.length; i++) {
            Visitados[i] = 0;
        }
        System.out.println("BFS");
        BFS(1, mat);

    }
}
