/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz;

import java.util.ArrayList;
import java.util.List;
import java.util.NavigableSet;
import java.util.Random;
import java.util.Scanner;
import java.util.TreeSet;

/**
 *
 * @author practica1
 */
public class BingoInfantil {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int casoPruebas;
        List<String> respuestas = new ArrayList<>();
        do {
            casoPruebas = leerEntero(sc);
            //casoPruebas = new Random().nextInt(1000)+1;
            if (casoPruebas == 0) {
                break;
            }
            TreeSet<Integer> bolas = new TreeSet<>();
            String[] numerosSTR = sc.nextLine().trim().split(" ");

            for (int i = 0; i < casoPruebas && numerosSTR.length >= casoPruebas; i++) {
                Integer dato = Integer.parseInt(numerosSTR[i]);
                bolas.add(dato);
            }
            /*for (int i = 0; i < casoPruebas; i++) {
                Integer dato = (new Random().nextInt(1000000) + 1);
                bolas.add(dato);
            }*/
            if (bolas.size() > 1) {
                StringBuilder sb = generarRespuesta(bolas);
                respuestas.add(sb.toString());
                if (casoPruebas % 10 == 0) {
                    casoPruebas = 0;
                }
            }
        } while (casoPruebas != 0);

        for (String respuesta : respuestas) {
            System.out.println(respuesta);
        }
    }

    private static int leerEntero(Scanner sc) {
        String str = sc.nextLine().trim().replaceAll("\\D+", "");
        int i = Integer.parseInt(str);
        return i;
    }

    private static StringBuilder generarRespuesta(TreeSet<Integer> bolas) {
        NavigableSet<Integer> resultados = new TreeSet<>();
        int cantBolas = bolas.size();
        while (bolas.size() > 1) {
            /*if (cantBolas <= resultados.size()) {
                break;
            }*/
            Integer mayor = bolas.pollLast();
            TreeSet<Integer> bolasBk = new TreeSet<>(bolas);
            do {
                resultados.add(mayor - bolasBk.pollFirst());
            } while (!bolasBk.isEmpty());
        }
        StringBuilder sb = new StringBuilder();
        for (Integer resultado : resultados) {
            if (sb.length() != 0) {
                sb.append(" ");
            }
            sb.append(resultado);
        }
        return sb;
    }
}