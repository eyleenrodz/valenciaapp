/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz;

import java.util.Arrays;
import static java.util.Arrays.sort;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

/**
 *
 * @author practica1
 */
public class MonedaGreedy {

    public static void main(String[] args) {
        int valor = 34;
        monedaGreedy(valor);
    }

    private static void monedaGreedy(int valor) {
        Integer monedas[] = {1, 5, 10, 25, 100};
        Integer resta = valor;
        Map<Integer, Integer> mapMoneda = new HashMap<>();
        Arrays.sort(monedas);
        TreeSet<Integer> listNumeros
                = new TreeSet<>(Arrays.asList(monedas));
        Integer mayor = -1;
        while (mayor != resta) {
            do {
                mayor = listNumeros.ceiling(resta);
                if (mayor == null) {
                    mayor = resta;
                }
                if (mayor < listNumeros.last()) {
                    listNumeros.remove(listNumeros.last());
                }
            } while (mayor < listNumeros.last());

            if (mayor != resta) {
                listNumeros.remove(mayor);
            }
            if (!listNumeros.isEmpty()) {
                Integer cantMoneda = null;
                cantMoneda = mapMoneda.get(listNumeros.last());
                if (cantMoneda == null) {
                    cantMoneda = 0;
                }
                mapMoneda.put(listNumeros.last(), cantMoneda+1);
                resta -= listNumeros.last();
            }else{
                break;
            }
        }
        for (Map.Entry<Integer, Integer> entry : mapMoneda.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            System.out.println("Moneda " + key + " cantidad " + value);
        }
    }

}
