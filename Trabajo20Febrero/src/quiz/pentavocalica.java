/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz;

import java.util.Scanner;

/**
 *
 * @author practica1
 */
public class pentavocalica {
    public static void main(String[] args) {
        ejecutarPentavocalica();
    }
    
    public static void ejecutarPentavocalica() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el número de iteraciones: ");
        
        int iteraciones = sc.nextInt();
        
        for (int i = 0; i < iteraciones; i++) {
            System.out.println("Ingrese la palabra a validar: ");
            
            boolean esPentavocalica = esPentavocalica(sc.next());
            
            if(esPentavocalica){
                System.out.println("SI");
            } else {
                System.out.println("NO");
            }
        }
    }
    
    public static boolean esPentavocalica(String cadena) {
        cadena = cadena.toUpperCase();
        String[] vocales = {"A", "E", "I", "O", "U"};
        
        boolean esPentavocalica = true;
        for (int i = 0; i < vocales.length; i++) {
            if(!cadena.contains(vocales[i])){
                esPentavocalica = false;
                break;
            }
        }
        
        return esPentavocalica;
    }
}
