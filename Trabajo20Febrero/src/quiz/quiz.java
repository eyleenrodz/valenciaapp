/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author practica1
 */
public class quiz {
    public static void main(String[] args) {
        // Quiz realizado pro Juan Carlos Altamar y Edwin Gutierrez Cervera
        
        abuelaFalsa();
        ejecutarPentavocalica();
    }
    
    public static void ejecutarPentavocalica() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el número de iteraciones: ");
        
        int iteraciones = sc.nextInt();
        
        for (int i = 0; i < iteraciones; i++) {
            System.out.println("Ingrese la palabra a validar: ");
            
            boolean esPentavocalica = esPentavocalica(sc.next());
            
            if(esPentavocalica){
                System.out.println("SI");
            } else {
                System.out.println("NO");
            }
        }
    }
    
    public static boolean esPentavocalica(String cadena) {
        cadena = cadena.toUpperCase();
        String[] vocales = {"A", "E", "I", "O", "U"};
        
        boolean esPentavocalica = true;
        for (int i = 0; i < vocales.length; i++) {
            if(!cadena.contains(vocales[i])){
                esPentavocalica = false;
                break;
            }
        }
        
        return esPentavocalica;
    }
    
    public static void abuelaFalsa() {
        Scanner sc = new Scanner(System.in);
        String linea = sc.nextLine();
        List<Boolean> resultados = new ArrayList<>();
        int n = 0;
        try {
            n = Integer.parseInt(linea);
            for (int i = 0; i < n; i++) {
                linea = sc.nextLine().toLowerCase();
                String[] arrStr = linea.split(" ");
                String nombreReal = arrStr[0];
                int cantNombres = Integer.parseInt(arrStr[1]);
                if (cantNombres > 1 && nombreReal.equalsIgnoreCase(arrStr[arrStr.length - 1]) && arrStr.length - 2 == cantNombres) {
                    resultados.add(Boolean.TRUE);
                } else {
                    resultados.add(Boolean.FALSE);
                }
            }
            for (Boolean resultado : resultados) {
                System.out.println(resultado ? "VERDADERA" : "FALSA");
            }
        } catch (NumberFormatException ex) {
            System.err.println("Se presentó un error con el valor ingresado, no es númerico");
        }
    }
}
