/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parcial;

import java.util.ArrayList;
import java.util.stream.IntStream;

/**
 *
 * @author practica1
 */
public class numerosNegativos {
    public static void main(String[] args) {
        ArrayList thisArrayList = new ArrayList();
        
        thisArrayList.add("Hi");
        thisArrayList.add("Hi");
        thisArrayList.add(",");
        thisArrayList.add("are");
        thisArrayList.add("you");
        thisArrayList.add("?");
        
        int[][] matriz=new int[3][5];
        matriz[0][0] = -2;
        matriz[0][1] = -1;
        matriz[0][2] = 0;
        matriz[0][3] = 2;
        matriz[0][4] = 3;
        matriz[1][0] = -1;
        matriz[1][1] = 1;
        matriz[1][2] = 2;
        matriz[1][3] = 7;
        matriz[1][4] = 9;
        matriz[2][0] = 0;
        matriz[2][1] = 0;
        matriz[2][2] = 10;
        matriz[2][3] = 11;
        matriz[2][4] = 12;
        
        int contador = 0;
        for (int i = 0; i < matriz.length; i++) {
            contador += IntStream.of(matriz[i]).filter(x -> x < 0).count();
        }
        
        System.out.println(contador);
    }
}
