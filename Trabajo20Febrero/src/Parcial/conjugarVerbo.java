/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parcial;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author practica1
 */
public class conjugarVerbo {

    public static void main(String[] args) {
        conjugarVerbos();
    }

    public static void conjugarVerbos() {
        Scanner sc = new Scanner(System.in);
        Boolean terminar = false;
        Map<String, List> verbos = new HashMap<String, List>();

        while (!terminar) {
            System.out.println("Ingrese el verbo y su tiempo, ejemplo 'saltar p'");
            String texto = sc.nextLine();
            String[] partes = texto.split(" ");
            List lista = verbos.get(partes[1].toUpperCase());
            if (lista == null) {
                lista = new ArrayList();
            }

            lista.add(partes[0].toUpperCase());
            verbos.put(partes[1].toUpperCase(), lista);
            
            if (!partes[1].toUpperCase().equals("P") && !partes[1].toUpperCase().equals("A") && !partes[1].toUpperCase().equals("F")) {
                terminar = true;
            }
        }
        for (Map.Entry<String, List> entry : verbos.entrySet()) {
            String key = entry.getKey();
            List value = entry.getValue();

            if (value.contains("SALTAR") && key.equals("A")) {
                System.out.println("Yo salto");
                System.out.println("Tu saltas");
                System.out.println("El salta");
                System.out.println("Nosotros saltamos");
                System.out.println("Vosotros saltais");
                System.out.println("Ellos saltan");
            } else if (value.contains("SALTAR") && key.equals("P")) {
                System.out.println("Yo salte");
                System.out.println("Tu saltaste");
                System.out.println("El salto");
                System.out.println("Nosotros saltamos");
                System.out.println("Vosotros saltasteis");
                System.out.println("Ellos saltaron");
            } else if (value.contains("SALTAR") && key.equals("F")) {
                System.out.println("Yo saltare");
                System.out.println("Tu saltaras");
                System.out.println("El saltara");
                System.out.println("Nosotros saltaremos");
                System.out.println("Vosotros saltarei");
                System.out.println("Ellos saltaran");
            } else if (value.contains("COMER") && key.equals("A")) {
                System.out.println("Yo como");
                System.out.println("Tu comes");
                System.out.println("El come");
                System.out.println("Nosotros comemos");
                System.out.println("Vosotros comeis");
                System.out.println("Ellos comen");
            } else if (value.contains("COMER") && key.equals("P")) {
                System.out.println("Yo comı");
                System.out.println("Tu comiste");
                System.out.println("El comio");
                System.out.println("Nosotros comimos");
                System.out.println("Vosotros comısteis");
                System.out.println("Ellos comieron");
            } else if (value.contains("COMER") && key.equals("F")) {
                System.out.println("Yo comere");
                System.out.println("Tu comeras");
                System.out.println("El comera");
                System.out.println("Nosotros comeremos");
                System.out.println("Vosotros comereis");
                System.out.println("Ellos comeran");
            } else if (value.contains("VIVIR") && key.equals("A")) {
                System.out.println("Yo vivo");
                System.out.println("Tu vives");
                System.out.println("El vive");
                System.out.println("Nosotros vivimos");
                System.out.println("Vosotros vivıs");
                System.out.println("Ellos viven");
            } else if (value.contains("VIVIR") && key.equals("P")) {
                System.out.println("Yo vivı");
                System.out.println("Tu viviste");
                System.out.println("El vivio");
                System.out.println("Nosotros vivimos");
                System.out.println("Vosotros vivısteis");
                System.out.println("Ellos vivieron");
            } else if (value.contains("VIVIR") && key.equals("F")) {
                System.out.println("Yo vivire");
                System.out.println("Tu viviras");
                System.out.println("El vivira");
                System.out.println("Nosotros viviremos");
                System.out.println("Vosotros vivireis");
                System.out.println("Ellos viviran");
            }
        }

    }
}
