/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parcial;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author practica1
 */
public class trim {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese la palabra a limpiar: ");
        String palabra = sc.nextLine();
        
        while (palabra.length() != 0) {
            limpiarPalabra(palabra);
            System.out.println("Ingrese la palabra a limpiar: ");
            palabra = sc.nextLine();
        }
    }

    public static void limpiarPalabra(String cadena) {
        String[] letras = cadena.split("");
        
        Set<String> caracteres = new HashSet<>();

        for (int i = 0; i < letras.length; i++) {
            caracteres.add(letras[i]);
        }
        
        System.out.println("El tim se ejecutara " + caracteres.size() + " veces");
    }
}
