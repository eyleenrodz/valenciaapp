/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package Parcial;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author eyleent.rodriguezr
 */
class Kaprekar {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = "";
        str = sc.nextLine();
        int numcasos = Integer.parseInt(str);
        List<Integer> lista = new ArrayList<>();
        for (int i = 0; i < numcasos; i++) {
            sc = new Scanner(System.in);
            str = "";
            str = sc.nextLine();
            int num = Integer.parseInt(str);
            if (num == 6174) {
                lista.add(0);
            }
            else{
                int cont = 0, val = 0;
                val = num;
                do{
                    String[] numeroAscendete = String.format("%04d", val).split("");
                    Arrays.sort(numeroAscendete);
                    String[] numeroDesc = orderDesc(numeroAscendete);
                    int asd = getInt(numeroAscendete);
                    int dsc = getInt(numeroDesc);
                    if(asd > dsc)
                        val = asd - dsc;
                    else 
                        val = dsc - asd;
                    cont++;
                }while(cont < 8 && val != 6174 );  
                if (val == 6174)
                    lista.add(cont);
                else 
                   lista.add(8); 
            }
        }
        for (int numero : lista) {
            System.out.println(numero);
        }
    }
    
    private static String[] orderDesc (String[] numero ){
        String[] num = new String [numero.length];
        for (int i = numero.length-1; i >= 0; i--) {
            num[numero.length-1-i] = numero[i];
        }
        return num;
    }
    
    private static int getInt (String[] numero){
        StringBuilder numstr = new StringBuilder();
        for (String numero1 : numero) {
            numstr.append(numero1);
        }
        return Integer.parseInt(numstr.toString());
    }
    
}
