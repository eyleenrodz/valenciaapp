/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parcial;

import java.text.NumberFormat;
import java.util.Scanner;

/**
 *
 * @author practica1
 */
public class potencias {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese la palabra los valores ejemplo(a b): ");
        String palabra = sc.nextLine();
        
        while (palabra.length() != 0) {
            try {
                potencia(palabra);
            } catch (NumberFormatException e) {
                System.out.println("Los datos ingresados no son numéricos");
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            } 
            
            System.out.println("Ingrese la palabra los valores ejemplo(a b): ");
            palabra = sc.nextLine();
        }
    }
    
    public static void potencia(String cadena) {
        String[] letras = cadena.split(" ");
        
        if(letras.length < 2){
            throw new IllegalArgumentException("El programa no se puede ejecutar no se presentaron los 2 argumentos");
        }
        
        int base = Integer.parseInt(letras[0]);
        int potencia = Integer.parseInt(letras[1]);
        
        int suma = 0;
        for (int i = 0; i <= potencia; i++) {
            suma += (int) Math.pow(base, i);
        }
        
        if(suma > 1000000){
            suma = suma % 1000007;
        }
    
        System.out.println(suma);
    }
}
