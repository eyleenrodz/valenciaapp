
import java.util.Scanner;
import java.util.List;
import negocio.*;
import DTO.*;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author eyleent.rodriguezr
 */
public class Inicio {
    static AdministradorUsuarios adUsuarios = new AdministradorUsuarios();
    static AdministradorProductos adProductos = new AdministradorProductos();
    static AdministradorVentas adVentas = new AdministradorVentas();
    static Scanner sc = new Scanner(System.in);
    static Cliente cliente;
    public static void main (String[] Args){
        int numeroOpcion  = 0;
        do{
            System.out.println("Bienvenido");
            System.out.println("Por favor, Ingrese id usuario ");
            int idUser = sc.nextInt();
            cliente = adUsuarios.consultarUsuario(idUser);
            if(cliente != null){
                do{
                    System.out.println("Hola "+ cliente.getNombreCliente());
                    System.out.println("Por favor digite la opción a la que quiere ingresar");
                    System.out.println("1 para Clientes");
                    System.out.println("2 para Productos");
                    System.out.println("3 para Ventas");
                    System.out.println("4 para Cerrar Sesion");
                    System.out.println("0 para Cerrar aplicación");
                    numeroOpcion = sc.nextInt();
                    switch(numeroOpcion){
                        case 1:
                            OpcionClientes();
                            break;
                        case 2:
                            OpcionProductos();
                            break;
                        case 3:
                            OpcionVentas();
                            break;
                        case 0:
                            break;
                        default:
                            System.out.println("Opción incorrecta");
                            numeroOpcion = 4;
                            break;

                   }
                }while(numeroOpcion != 4 && numeroOpcion != 0 );
            }
            else
                System.out.println("No se encontro Usuario");
        }while(numeroOpcion != 0);
        System.out.println("Hasta Luego!");
    }

    private static void OpcionClientes(){
         int opcionCliente, idUser;
         Cliente cl;
         do{
             System.out.println("Selecciona la opción a la que desea ingresar");
             System.out.println("1 para Crear cliente");
             System.out.println("2 para consultar cliente por identificador");
             System.out.println("3 para consultar todos los clientes");
             System.out.println("4 para editar un cliente");
             System.out.println("4 para eliminar un cliente");
             System.out.println("0 para regresar al menu anterior");
             opcionCliente = sc.nextInt();
             switch(opcionCliente){
                 case 1:
                     System.out.println("Ingrese el nombre del cliente");
                     sc = new Scanner(System.in);
                     String user = sc.nextLine();
                     adUsuarios.crearUsuario(user);
                     System.out.println("Cliente creado exitosamente");
                     sc.nextLine();
                     break;
                 case 2:
                     System.out.println("Ingrese el identificador del cliente");
                     idUser = sc.nextInt();
                     cl = adUsuarios.consultarUsuario(idUser);
                     System.out.println(cl.toString());
                     sc.nextLine();
                     break;
                 case 3:
                     List<Cliente> usuarios = adUsuarios.consultarUsuarios();
                     if(usuarios != null && usuarios.size()>0){
                     usuarios.forEach(r->System.out.println( r.toString()));
                     }
                     else
                         System.out.println("No se encontraron clientes registrados");
                     sc.nextLine();
                     break;
                 case 4:
                     System.out.println("Digite Identificador");
                     idUser = sc.nextInt();
                     cl = adUsuarios.consultarUsuario(idUser);
                     if(cl != null ){
                         System.out.println("Digite el nombre a cambiar");
                         String nombre = sc.next();
                         sc = new Scanner(System.in);
                         System.out.println("¿Desea confirmar el cambio? Y: si | N: no");
                         String letra = sc.nextLine();
                         if (letra.toUpperCase().charAt(0) == 'Y'){
                             adUsuarios.editarUsuario(idUser, nombre);
                             System.out.println("Cambio Exitoso");
                         }
                         sc.nextLine();
                     }
                     break;
                 case 5:
                     System.out.println("Digite Identificador");
                     idUser = sc.nextInt();
                     adUsuarios.eliminarUsuario(idUser);
                     System.out.println("Se ha eliminado cliente");
                     sc.nextLine();
                     break;
                 case 0:
                     break;
                 default:
                     System.out.println("Opción Incorrecta");
                     opcionCliente =0;
                     break;
             }

         }while (opcionCliente != 0);
    }
    private static void OpcionProductos(){
        int opcionProducto, cantidad, id;
        String nombre;
        Double precio, monto;
        sc = new Scanner(System.in);
         Producto product;
         do{
             System.out.println("Selecciona la opción a la que desea ingresar");
             System.out.println("1 para crear un producto");
             System.out.println("2 para consultar los productos");
             System.out.println("3 para editar los productos");
             System.out.println("0 para regresar al menu anterior");
             opcionProducto = sc.nextInt();
             switch(opcionProducto){
                 case 1:
                    System.out.println("Ingrese el nombre del producto");sc = new Scanner(System.in);
                    nombre = sc.nextLine();
                    System.out.println("Ingrese la cantidad del producto");
                    cantidad = sc.nextInt();
                    System.out.println("Ingrese el precio del producto");
                    precio = sc.nextDouble();
                    System.out.println("Ingrese el monto del producto");
                    monto = sc.nextDouble();
                    adProductos.crearProducto(nombre, cantidad, precio, monto);
                    System.out.println("Producto creado exitosamente");
                    sc.nextLine();
                    break;
                 case 2:
                     List<Producto> productos = adProductos.consultarProductos();
                     if(productos != null){
                         productos.forEach(r-> System.out.println(r.toString()));
                     }
                     else
                         System.out.println("No hay productos");
                     sc.nextLine();
                     break;
                 case 3:
                    System.out.println("Ingrese el identificador del producto");
                    id = sc.nextInt();
                    System.out.println("Ingrese nuevo nombre del producto");
                    sc = new Scanner(System.in);
                    nombre = sc.nextLine();
                    System.out.println("Ingrese nueva cantidad del producto");
                    cantidad = sc.nextInt();
                    System.out.println("Ingrese nuevo precio del producto");
                    precio = sc.nextDouble();
                    System.out.println("Ingrese nuevo monto del producto");
                    monto = sc.nextDouble();
                    adProductos.editarProducto(id,nombre, cantidad, precio, monto);
                    System.out.println("Producto modificado exitosamente");
                    sc.nextLine();
                     break;
                 default:
                     if(opcionProducto != 0)
                        System.out.println("Opción Incorrecta");
                     opcionProducto = 0;
                     break;
             }

         }while (opcionProducto != 0);
    }
    private static void OpcionVentas(){
        int opcionVenta;
        sc = new Scanner(System.in);
         Producto product;
         do{
             System.out.println("Selecciona la opción a la que desea ingresar");
             System.out.println("1 para crear una Venta");
             System.out.println("2 para consultar las venas realizadas");
             System.out.println("0 para regresar al menu anterior");
             opcionVenta = sc.nextInt();
             switch(opcionVenta){
                 case 1:
                    try{
                        if (cliente != null){
                            int bloqueadoB = cliente.getNombreCliente().toUpperCase().indexOf("BRAYAN")+1;
                            int bloqueadoJ = cliente.getNombreCliente().toUpperCase().indexOf("JULIETH")+1;

                            if (bloqueadoB == 0 && bloqueadoJ == 0){
                                Venta objVenta = new Venta();
                                Producto productoCompra;
                                Boolean productos = true;
                                do{
                                    productoCompra = null;
                                    System.out.println("Ingrese identificador del producto");
                                    int idPro = sc.nextInt();
                                    productoCompra = adProductos.consultar(idPro);
                                    if (productoCompra == null || productoCompra.getCantidad() == 0){
                                        System.out.println("No se encontro producto o está Agotado");
                                    }
                                    else{
                                        DetalleVenta objDetalle = new DetalleVenta();
                                        objDetalle.setProducto(productoCompra);
                                        objDetalle.setCliente(cliente);
                                        System.out.println("Ingrese la cantidad del producto");
                                        int cantidadPro = sc.nextInt();
                                        if((productoCompra.getCantidad()-cantidadPro) < 0) break;
                                        objDetalle.setCantidad(cantidadPro);
                                        System.out.println("valor: "+objDetalle.getValorUnitario());
                                        objVenta.addDetalleVenta(objDetalle);
                                    }
                                    System.out.println("¿Desea agregar otro producto? Y:si | N: no");
                                    String resp =sc.next();
                                    productos = resp.toUpperCase().charAt(0) == 'Y';
                                }while (productos);
                                if(objVenta.getDetalles().size()> 0){
                                    System.out.println("total: "+objVenta.getTotal());
                                    System.out.println("Por favor ingrese porcentaje de impuesto sin el simbolo %");
                                    double varimpuesto= sc.nextDouble()*0.01;
                                    objVenta.setImpuesto(varimpuesto);
                                    System.out.println("Por favor ingrese porcentaje de descuento sin el simbolo %");
                                    double vardescuento= sc.nextDouble()*0.01;
                                    objVenta.setDescuento(vardescuento);

                                    System.out.println("Total de: "+objVenta.getTotal());
                                    System.out.println("¿Desea guardar la venta?");
                                    System.out.println("presione Y para guardar o cualquier otra letra para cancelar");
                                    sc = new Scanner(System.in);
                                    String continuar = sc.nextLine();
                                    if (continuar.toUpperCase().charAt(0) == 'Y'){
                                        adVentas.agregarVenta(objVenta);
                                        actualizarProductos();
                                        System.out.println("venta guardada");
                                    }
                                }
                                else{
                                    System.out.println("Venta finalizada");
                                }

                            }
                            else {
                                throw new Exception("Nos reservamos el derecho de admisión");
                            }
                        }
                    }
                    catch(Exception ex){
                        System.out.println("Error: "+ex.getMessage());
                    }
                    break;
                 case 2:
                     List<Venta> ventas = adVentas.consultarVentas();
                     ventas.forEach(r-> {
                         System.out.println("Identificador: "+r.getId());
                         System.out.println("Producto       PrecioUnidad ");
                         r.getDetalles().forEach(d->System.out.println(d.toString()) );
                         System.out.println("SubTotal : "+r.getSubTotal());
                         System.out.println("Impuesto : "+r.getImpuesto());
                         System.out.println("Descuento : "+r.getDescuento());
                         System.out.println("Total : "+r.getTotal());

                     });
                     sc.nextLine();
                     break;
                 default:
                     if(opcionVenta != 0)
                        System.out.println("Opción Incorrecta");
                     opcionVenta = 0;
                     break;
             }

         }while (opcionVenta != 0);
    }

    private static void actualizarProductos(){
        List<Venta> ventas = adVentas.consultarVentas();
        for (Venta objVenta : ventas) {
            objVenta.getDetalles().forEach(r-> adProductos.editarCantidadProducto(r.getProducto().getId(), r.getCantidad()));
        }
    }
}
