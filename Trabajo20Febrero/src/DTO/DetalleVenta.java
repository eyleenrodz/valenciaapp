/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author practica1
 */
public class DetalleVenta {

  private int id;
  private Cliente cliente;
  private Producto producto;
  private int cantidadVenta;

  public DetalleVenta(){

  }
  /**
   * @return the id
   */
  public int getId() {
      return id;
  }

  /**
   * @return the cliente
   */
  public Cliente getCliente() {
      return cliente;
  }

  /**
   * @param cliente the cliente to set
   */
  public void setCliente(Cliente cliente) {
      this.cliente = cliente;
  }

  /**
   * @return the producto
   */
  public Producto getProducto() {
      return producto;
  }

  /**
   * @param producto the producto to set
   */
  public void setProducto(Producto producto) {
      this.producto = producto;
  }

  /**
   * @param cliente the cliente to set
   */
  public void setCantidad(int cantidad) {
      this.cantidadVenta = cantidad;
  }

  /**
   * @return the producto
   */
  public int getCantidad() {
      return cantidadVenta;
  }

  public double getValorUnitario(){
    return this.producto.getPrecio() * this.cantidadVenta;
  }

  @Override
  public String toString(){
    return this.producto.getNombre()+ "-"+ this.cantidadVenta+ "    "+ this.producto.getPrecio();
  }
}
