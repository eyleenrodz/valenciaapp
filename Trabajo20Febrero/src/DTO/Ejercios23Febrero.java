/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author eyleent.rodriguezr
 */
public class Ejercios23Febrero {
    static Scanner sc = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        diasFaltantes();
    }
    private static void SumaDigitos(){
        List<String> lista = new ArrayList<>();
        do{
            int num = sc.nextInt();
            if(num<=0) break;
            int total = 0;
            String str= "";
            String[] strnum = String.valueOf(num).split("");
            for (int i = 0; i< strnum.length; i++) {
                if (i == strnum.length-1){
                    str = str + strnum[i] + " = ";
                }
                else 
                    str = str + strnum[i] + " + ";
                total += Integer.parseInt(strnum[i]);
            }
            str += total;
            lista.add(str);
        }while(true);
        lista.forEach(r->System.out.println(r));
        
    }
    
    
    private static void VueltaCiclista(){
        int casos = sc.nextInt();
        int[] resp = new int[casos];
        for (int i = 0; i < casos; i++) {
            int suma = 0,a = 0, b= 0;
            do{
                int input = sc.nextInt();
                if(input == 0) break;
                a = suma;
                b = b+input;
                suma = a +b;
            }while(true);
            resp[i]= suma;
        }
        
        for (int j = 0; j < resp.length; j++) {
            System.out.println(resp[j]*2);
        }
    }
    
    private static void diasFaltantes(){
        int a = sc.nextInt();
        List<Integer> diasF = new ArrayList<>();
        for (int i = 0; i < a; i++) {
            int day = sc.nextInt();
            int month = sc.nextInt();
            LocalDate fechaIni = LocalDate.of(2018,day,month);
            LocalDate fechafin = LocalDate.of(2018,12,31);

            int dayi = fechaIni.getDayOfYear();
            int dayf = fechaIni.getDayOfYear();
            diasF.add(dayi+dayf);
        }
        diasF.forEach(r-> System.out.println(r));
    }
    
}
