/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author practica1
 */
public class Venta {

  private int id;
  private List<DetalleVenta> detalles;
  private double subTotal;
  private double impuesto;
  private double descuento;
  private String total;

  public Venta(){
    this.detalles = new ArrayList<>();
  }

  /**
   * @return the id
   */
  public int getId() {
      return id;
  }

  public void setId(int id){
    this.id = id;
  }

  /**
   * @return the venta
   */
  public List<DetalleVenta> getDetalles() {
      return detalles;
  }

  /**
   * @param venta the venta to set
   */
  public void addDetalleVenta(DetalleVenta detalleVenta) {
    if (detalleVenta.getProducto() != null){
      this.detalles.add(detalleVenta);
      this.subTotal += detalleVenta.getValorUnitario();
    }

  }

  /**
   * @return the subTotal
   */
  public double getSubTotal() {
      return subTotal;
  }

  /**
   * @return the impuesto
   */
  public double getImpuesto() {
      return impuesto;
  }

  /**
   * @param impuesto the impuesto to set
   */
  public void setImpuesto(double impuesto) {
      this.impuesto = impuesto;
  }

  /**
   * @return the descuento
   */
  public double getDescuento() {
      return descuento;
  }

  /**
   * @param descuento the descuento to set
   */
  public void setDescuento(double descuento) {
      this.descuento = descuento;
  }

  /**
   * @return the total
   */
  public String getTotal() {
      double totalImpuesto = this.subTotal + (this.subTotal*this.impuesto);
      double totalDesc = totalImpuesto-(totalImpuesto*this.descuento);
      return String.valueOf(totalDesc);
  }



}
