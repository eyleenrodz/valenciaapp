/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Marzo9;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author eyleent.rodriguezr
 */
public class SeriePow {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        List<Double> lista = new ArrayList();
        String input = "";
       do{
           input = sc.nextLine();
           if(!input.isEmpty()){
               String[] arreglo1 = input.split(" ");
               int base = Integer.parseInt(arreglo1[0]);
               int expoF = Integer.parseInt(arreglo1[1]);
               double result = 0;
               for (int i = 0; i <= expoF; i++) {
                   //System.out.println(Math.pow(base, i));
                   result += Math.pow(base, i);
               }
               lista.add((result%1000007));               
           }
       }while(!input.isEmpty());
       lista.forEach(r-> System.out.println(String.format("%.0f",r)));
    }
    
}
