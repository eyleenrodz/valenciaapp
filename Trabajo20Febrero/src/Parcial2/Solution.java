
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/*
 * J-Sum
 */
/**
 *
 * @author practica1
 */
class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int cantCases = leerEntero(sc);
        List<Integer> listEnteros = new ArrayList<>();
        for (int i = 0; i < cantCases; i++) {
            listEnteros.add(leerEntero(sc));
        }
        List<String> listResultados = new ArrayList<>();
        for (Integer entero : listEnteros) {
            listResultados.add(calcularSolucion(entero));
        }
        imprimirResultado(listResultados);
    }

    private static int leerEntero(Scanner sc) {
        String str = sc.nextLine().trim().replaceAll("\\D+", "");
        int i = Integer.parseInt(str);
        return i;
    }

    private static String calcularSolucion(Integer entero) {
        int numeroInicial = entero/2;
        boolean encontrado = false;
        List<Integer> listNumeros;
        int sum = 0;
        do {
            listNumeros = new ArrayList<>();
            sum = 0;
            for (int i = numeroInicial; i >0; i--) {
                sum += i;
                listNumeros.add(i);
                if (entero.equals(sum)) {
                    encontrado = true;
                    break;
                }else if(sum>entero){
                    numeroInicial--;
                    break;
                }
            }
        } while (!encontrado && numeroInicial>0 && sum>entero);
        Collections.reverse(listNumeros);
        StringBuilder sb = new StringBuilder();
        if (encontrado) {
            sb.append(entero);
            sb.append(" =");
            for (Integer numero : listNumeros) {
                if (!sb.toString().endsWith("=")) {
                    sb.append(" +");
                }
                sb.append(" ").append(numero);
            }
        } else {
            sb.append("IMPOSSIBLE");
        }
        return sb.toString();
    }

    private static void imprimirResultado(List<String> listResultados) {
        for (String resultado : listResultados) {
            System.out.println(resultado);
        }
    }
}
