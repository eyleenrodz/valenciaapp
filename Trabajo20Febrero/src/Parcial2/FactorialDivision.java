/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parcial2;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author practica1
 */
public class FactorialDivision {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String mensaje = "No se entregaron los valores necesarios";
        String texto = sc.nextLine();

        String[] strnumeros = texto.split(" ");
        try {
            int num = Integer.parseInt(strnumeros[0]);
            int den = Integer.parseInt(strnumeros[1]);
            List<BigInteger> listaResultados = new ArrayList<>();
            while (num > den) {
                listaResultados.add(factorialDivision(num, den));
                texto = sc.nextLine();
                strnumeros = texto.split(" ");

                if (strnumeros.length > 2) {
                    throw new IllegalArgumentException(mensaje);
                }

                num = Integer.parseInt(strnumeros[0]);
                den = Integer.parseInt(strnumeros[1]);
            }
            for (BigInteger listaResultado : listaResultados) {
                System.out.println("");
            }
        } catch (Exception e) {
            System.err.println(mensaje);
        }
    }

    public static BigInteger factorialDivision(int num, int den) {
        BigInteger numeroFactorial = new BigInteger("1");
        BigInteger dividirFactorial = new BigInteger("1");

        for (int i = num; i > 0; i--) {
            numeroFactorial = numeroFactorial.multiply(new BigInteger(String.valueOf(i)));
        }

        for (int i = den; i > 0; i--) {
            dividirFactorial = dividirFactorial.multiply(new BigInteger(String.valueOf(i)));
        }

        return (numeroFactorial.divide(dividirFactorial));
    }
}
