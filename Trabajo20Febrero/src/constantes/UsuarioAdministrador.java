/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantes;

import DTO.Cliente;

/**
 *
 * @author practica1
 */
public enum UsuarioAdministrador {
    ADMINISTRADOR(1, "ADMIN");

    private int id;
    private String nombre;
    private Cliente cliente;

    private UsuarioAdministrador(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
        this.cliente = new Cliente(id, nombre);
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public Cliente getCliente() {
        return cliente;
    }

}
