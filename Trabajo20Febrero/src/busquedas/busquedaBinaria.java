/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package busquedas;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 *
 * @author practica1
 */
public class busquedaBinaria {

    public static void main(String[] args) {
        int arreglo[] = new int[11];
        arreglo[0] = 2;
        arreglo[1] = 1;
        arreglo[2] = 0;
        arreglo[3] = 3;
        arreglo[4] = 6;
        arreglo[5] = 5;
        arreglo[6] = 4;
        arreglo[7] = 9;
        arreglo[8] = 8;
        arreglo[9] = 7;

        int posicion = buscar(arreglo, 7);
        System.out.println("La posición es: " + posicion);

        lineal();
    }

    static int buscar(int[] arreglo, int dato) {
        int inicio = 0;
        int fin = arreglo.length - 1;
        int pos;
        while (inicio <= fin) {
            pos = (inicio + fin) / 2;
            if (arreglo[pos] == dato) {
                return pos;
            } else if (arreglo[pos] < dato) {
                inicio = pos + 1;
            } else {
                fin = pos - 1;
            }
        }
        return -1;
    }

    public static void lineal() {
        int l[] = {10, 58, 69, 23, 56, 74, 96, 14, 100, 25, 79};
        int pos = -1;
        int busqueda = 15888;
        for (int i = 0; i < l.length; i++) {
            if (busqueda == l[i]) {
                pos = i;
            }
        }
        if (pos != -1) {
            System.out.println("El elemento se encuentra en la posición " + pos);
        } else {
            System.out.println("El elemento no existe");
        }
    }

    public static void hasShing(String args[]) throws Exception {

        Hashtable hash = new Hashtable(10, 10);
        for (int i = 0; i <= 100; i++) {
            Integer entero = new Integer(i);
            hash.put(entero, "Numero : " + i);
        }
        for (Enumeration e = hash.keys(); e.hasMoreElements();) {
            System.out.println(hash.get(e.nextElement()));
        }
    }

}
