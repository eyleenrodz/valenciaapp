/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JuanKRetos.Skynet;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author jaltamar
 */
class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt(); // the total number of nodes in the level, including the gateways
        System.err.println(N);
        int L = in.nextInt(); // the number of links
        System.err.println(L);
        int E = in.nextInt(); // the number of exit gateways
        System.err.println(E);
        if (estaEnElRango(N, 2, 500) && estaEnElRango(L, 1, 1000) && estaEnElRango(E, 1, 20)) {
            Map<Integer, Boolean> mapLinks[] = new HashMap[N];
            for (int i = 0; i < L; i++) {
                int N1 = in.nextInt(); // N1 and N2 defines a link between these nodes
                int N2 = in.nextInt();
                mapLinks[N1] = relacionar(mapLinks, N1, N2);
                mapLinks[N2] = relacionar(mapLinks, N2, N1);
                System.err.println(N1 + " " + N2);
            }
            Set<Integer> setNodoGateway = new HashSet<>();
            for (int i = 0; i < E; i++) {
                int EI = in.nextInt(); // the index of a gateway node
                setNodoGateway.add(EI);
                System.err.println(EI);
            }

            // game loop            
            while (true) {
                int SI = in.nextInt(); // The index of the node on which the Skynet agent is positioned this turn
                System.err.println(SI);
                Deque<Integer> pila = breathFirstSearchChild(mapLinks, SI, setNodoGateway);
                int meta = pila.pop();
                int hijoMeta = pila.pop();

                Map<Integer, Boolean> hijosMeta = mapLinks[hijoMeta];
                hijosMeta.put(meta, Boolean.TRUE);
                if (meta > hijoMeta) {
                    System.out.println(hijoMeta + " " + meta);
                } else {
                    System.out.println(meta + " " + hijoMeta);
                }
                setNodoGateway.add(meta);
            }
        }
    }

    public static boolean estaEnElRango(int valor, int min, int max) {
        return min <= valor && valor <= max;
    }

    private static Deque<Integer> breathFirstSearchChild(Map<Integer, Boolean>[] mapLinks, int indexSkynet, Set<Integer> setNodoGateway) {
        Queue<Integer> q = new LinkedList<>();
        boolean visited[] = new boolean[mapLinks.length];
        q.add(indexSkynet);
        visited[indexSkynet] = true;
        Deque<Integer> pila = new ArrayDeque<>();
        while (!q.isEmpty()) {
            int nodo = q.poll();
            pila.add(nodo);
            for (Map.Entry<Integer, Boolean> entry : mapLinks[nodo].entrySet()) {
                int index = entry.getKey();
                if (!visited[index] && !entry.getValue()) {
                    visited[index] = true;
                    q.add(index);
                    if (setNodoGateway.contains(index)) {
                        pila.add(index);
                        return gestionarPila(mapLinks, pila);
                    }
                }
            }
        }
        return pila;
    }

    private static Map<Integer, Boolean> relacionar(Map<Integer, Boolean>[] mapLinks, int N1, int N2) {
        if (mapLinks[N1] == null) {
            mapLinks[N1] = new HashMap<>();
        }
        mapLinks[N1].put(N2, false);
        return mapLinks[N1];
    }

    private static Deque<Integer> gestionarPila(Map<Integer, Boolean>[] mapLinks, Deque<Integer> pila) {
        List<Integer> lista = new ArrayList<>(pila);
        Collections.reverse(lista);
        List<Integer> caminoCorto = new ArrayList<>();
        int origen = lista.get(0);
        int nodoAux = origen;
        int destino = lista.get(lista.size() - 1);
        caminoCorto.add(origen);
        for (int i = 1; i < lista.size(); i++) {
            if (mapLinks[nodoAux].get(lista.get(i)) != null) {
                nodoAux = lista.get(i);
                caminoCorto.add(nodoAux);
            }
        }
        Iterator<Integer> iter = caminoCorto.iterator();
        while (iter.hasNext()) {
            int nodo = iter.next();
            if (destino != nodo) {
                for (Map.Entry<Integer, Boolean> hijo : mapLinks[nodo].entrySet()) {
                    if (hijo.getValue()) {
                        caminoCorto = caminoCorto.subList(caminoCorto.indexOf(nodo), caminoCorto.size());
                    }
                }
            }
        }
        Deque<Integer> nuevaPila = new ArrayDeque<>(caminoCorto);
        return nuevaPila;
    }

}

class Nodo {

    int index;
    boolean visitado;

    public Nodo(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setVisitado(boolean visitado) {
        this.visitado = visitado;
    }

    public boolean isVisitado() {
        return visitado;
    }
}
