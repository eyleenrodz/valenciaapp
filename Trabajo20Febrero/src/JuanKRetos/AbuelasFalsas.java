/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JuanKRetos;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author practica1
 */
public class AbuelasFalsas {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String linea = sc.nextLine();
        List<Boolean> resultados = new ArrayList<>();
        int n = 0;
        try {
            n = Integer.parseInt(linea);
            for (int i = 0; i < n; i++) {
                linea = sc.nextLine().toLowerCase();
                String[] arrStr = linea.split(" ");
                String nombreReal = arrStr[0];
                int cantNombres = Integer.parseInt(arrStr[1]);
                if (cantNombres > 1 && nombreReal.equalsIgnoreCase(arrStr[arrStr.length - 1]) && arrStr.length - 2 == cantNombres) {
                    resultados.add(Boolean.TRUE);
                } else {
                    resultados.add(Boolean.FALSE);
                }
            }
            for (Boolean resultado : resultados) {
                System.out.println(resultado ? "VERDADERA" : "FALSA");
            }
        } catch (NumberFormatException ex) {
            System.err.println("Se presentó un error con el valor ingresado, no es númerico");
        }
    }

}
