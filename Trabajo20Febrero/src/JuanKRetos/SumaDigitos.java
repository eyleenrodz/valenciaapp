/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JuanKRetos;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author practica1
 */
public class SumaDigitos {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numero = sc.nextInt();
        List<String> listSumas = new ArrayList<>();
        while (numero > -1) {
            int suma = 0;
            StringBuilder sb = new StringBuilder();
            char[] arrChar = String.valueOf(numero).toCharArray();
            for (int i = 0; i < arrChar.length; i++) {
                suma += arrChar[i];
                sb.append(arrChar[i]);
                sb.append(" ");
                if (i == arrChar.length - 1) {
                    break;
                }
                sb.append(" + ");                
            }
            sb.append(" = ");
            sb.append(suma);
            listSumas.add(sb.toString());
            numero = sc.nextInt();
        }

        for (String listSuma : listSumas) {
            System.out.println(listSuma);
        }
    }
}
