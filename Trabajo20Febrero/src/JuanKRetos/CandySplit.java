/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JuanKRetos;

/**
 *
 * @author practica1
 */
public class CandySplit {

    public static void main(String[] args) {
        int[] numeros = new int[]{3, 5, 6};
        Integer acum = null;
        for (int numero : numeros) {
            if (acum == null) {
                acum = numero;
            } else {
                acum = acum ^ numero;
            }
        }
        System.out.println(acum);
    }
}
