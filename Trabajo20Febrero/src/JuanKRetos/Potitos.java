/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package JuanKRetos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author practica1
 */
/*public*/ class Potitos {

    public static void main(String[] args) {
        int numeroPotitosDados = 0;//Max 25
        String comioPotito = null;//SI: o NO:
        List<String> listIngredientesSiComio = null;//Max 10, cada ingrediente length 20 LowerCase
        List<String> listIngredientesNoComio = null;//Max 10, cada ingrediente length 20 LowerCase
        List<String> listResultados = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        numeroPotitosDados = Integer.parseInt(sc.nextLine());
        while (numeroPotitosDados > 0) {
            listIngredientesSiComio = new ArrayList<>();
            listIngredientesNoComio = new ArrayList<>();
            for (int i = 0; i < numeroPotitosDados; i++) {
                comioPotito = sc.next().toUpperCase();
                switch (comioPotito) {
                    case "SI:":
                        listIngredientesSiComio = llenarIngredientes(sc, listIngredientesSiComio);
                        break;
                    case "NO:":
                        listIngredientesNoComio = llenarIngredientes(sc, listIngredientesNoComio);
                        break;
                    default:
                        break;
                }
            }
            Collections.sort(listIngredientesSiComio);
            Collections.sort(listIngredientesNoComio);
            String ingredientesEncontrados = encontrarIngredientes(listIngredientesSiComio, listIngredientesNoComio);
            listResultados.add(ingredientesEncontrados);
            numeroPotitosDados = sc.nextInt();
        }

        for (String listResultado : listResultados) {
            System.out.println(listResultado);
        }
    }

    private static List<String> llenarIngredientes(Scanner sc, List<String> listIngredientes) {
        if (listIngredientes == null) {
            listIngredientes = new ArrayList<>();
        }
        if (sc.hasNext()) {
            String ingrediente;
            do {
                ingrediente = sc.next();
                if (!"FIN".equalsIgnoreCase(ingrediente)) {
                    listIngredientes.add(ingrediente.toLowerCase());
                } else {
                    break;
                }
            } while (sc.hasNext());
        }
        return listIngredientes;
    }

    private static String encontrarIngredientes(List<String> listIngredientesSiComio, List<String> listIngredientesNoComio) {
        StringBuilder sb = new StringBuilder();
        for (String ingrediente : listIngredientesNoComio) {
            if (!listIngredientesSiComio.contains(ingrediente)) {
                if (sb.length() > 0) {
                    sb.append(" ");
                }
                sb.append(ingrediente);
            }
        }
        return sb.toString();
    }

}
