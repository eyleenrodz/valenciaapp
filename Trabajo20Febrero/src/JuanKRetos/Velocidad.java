/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JuanKRetos;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author practica1
 */
public class Velocidad {

    private Double velocidad;
    private Double desplazamiento;
    private Double tiempo;

    public void calcularDatoFaltante() {
        if (velocidad == null && desplazamiento != null && tiempo != null) {
            velocidad = desplazamiento / tiempo;
            System.out.println(String.format("V=%.0f", velocidad));
        } else if (desplazamiento == null && tiempo != null && velocidad != null) {
            desplazamiento = velocidad * tiempo;
            System.out.println(String.format("D=%.0f", desplazamiento));
        } else if (tiempo == null && desplazamiento != null && velocidad != null) {
            tiempo = desplazamiento / velocidad;
            System.out.println(String.format("T=%.0f", tiempo));
        }
    }

    private static Velocidad generarPregunta(String datoLeido) {
        Velocidad pregunta = new Velocidad();
        String[] datos = datoLeido.toUpperCase().split(" ");
        if (datos.length < 2) {
            throw new IllegalArgumentException("Se deben ingresar mínimo dos valores para el programa");
        }
        for (String dato : datos) {
            String[] palabras = dato.toUpperCase().split("=");
            double intDato = Double.parseDouble(palabras[1]);
            switch (palabras[0]) {
                case "V":
                    pregunta.velocidad = intDato;
                    break;
                case "T":
                    pregunta.tiempo = intDato;
                    break;
                case "D":
                    pregunta.desplazamiento = intDato;
                    break;
                default:
                    throw new IllegalArgumentException("El dato de la pregunta debe iniciar con V,T o D seguido por un igual");
            }
        }

        return pregunta;
    }

    public static void main(String[] args) {
        int numPreguntas = 0;//Número de preguntas
        Scanner lector = new Scanner(System.in);
        String datoLeido = lector.nextLine();
        try {
            if (!datoLeido.isEmpty()) {
                numPreguntas = Integer.parseInt(datoLeido);
                List<Velocidad> lista = new ArrayList<>();
                for (int i = 0; i < numPreguntas; i++) {
                    datoLeido = lector.nextLine();
                    if (datoLeido.isEmpty() || datoLeido.trim().isEmpty()) {
                        throw new IllegalArgumentException("Línea de pregunta invalida");
                    } else {
                        Velocidad pregunta = generarPregunta(datoLeido);
                        lista.add(pregunta);
                    }
                }
                for (Iterator<Velocidad> iterator = lista.iterator(); iterator.hasNext();) {
                    Velocidad next = iterator.next();
                    next.calcularDatoFaltante();
                }
            } else {
                throw new IllegalArgumentException("La linea ingresada se encuentra vacia");
            }
        } catch (NumberFormatException ex) {
            System.err.println("Se ingresó un valor invalido\nSe esperaba un valor numerico");
        } catch (IllegalArgumentException ex) {
            System.err.println(ex.getMessage());
        }

    }
}
